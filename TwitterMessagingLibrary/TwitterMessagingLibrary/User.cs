﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TwitterMessagingLibrary
{
    public class User 
    {
        public String username;
        public LinkedList<User> followers;
        public LinkedList<User> following;
        public LinkedList<Tweet> tweets;
        public int tweetCount;

        public User(String userN) {
            username = userN;
            tweetCount = 0;

        }

        // override object.Equals
    public override bool Equals(object obj)
        {
            //       
            // See the full list of guidelines at
            //   http://go.microsoft.com/fwlink/?LinkID=85237  
            // and also the guidance for operator== at
            //   http://go.microsoft.com/fwlink/?LinkId=85238
            //

            if (obj == null || GetType() != obj.GetType())
            {
                return false;
            }

            // TODO: write your implementation of Equals() here
            User obj1 = obj as User;
            if (username != obj1.username)
            {
                return false;
            }
            else {
                return true;
            }
        }
        
    }
}
