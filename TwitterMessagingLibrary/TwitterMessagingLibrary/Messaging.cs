﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TwitterMessagingLibrary
{
    public class Messaging
    {
        public LinkedList<User> Users;


        public void addUser(User person) {
            Users.AddLast(person);
        }

        public Boolean userExists(String userN)
        {
            User test = new User(userN);
            if (Users.Contains(test))
            {
                return true;
            }
            else {
                return false;
            }

        }

        public User returnUser(String userN)
        {
            User test = new User(userN);
            return Users.Find(test).Value;

        }


    }
}
