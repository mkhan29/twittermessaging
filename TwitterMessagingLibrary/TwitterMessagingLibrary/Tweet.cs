﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TwitterMessagingLibrary
{
    public class Tweet
    {
        public String message;
        public LinkedList<User> mentions;
        public LinkedList<String> links;
        public LinkedList<String> topics;
        public LinkedList<User> favorites;
        public LinkedList<User> retweets;
        public Boolean deleted;

        //After user hits send, this is the test tweet must pass to be posted
        public Boolean validTweet(String text) {
            if (text.Length <= 140 && text.Length != 0)
            {
                return true;
            }
            else {
                return false;
            }

        }

        public void mentionSearch(String text) {
            
            LinkedList<User> tempMent = new LinkedList<User>();
            while (text.IndexOf("@") != -1) {
                int stop = text.IndexOf(" ", text.IndexOf("@")) - text.IndexOf("@");
                String ment = text.Substring(text.IndexOf("@"), stop);
                text.Remove(text.IndexOf("@"), stop);
                //create method that checks if user is real
                User tU = new User(ment);
                tempMent.AddLast(tU);

            }
            mentions = tempMent;

        }

        public void topicSearch(String text) {
            LinkedList<String> tempTop = new LinkedList<String>();
            while (text.IndexOf("#") != -1)
            {
                int stop = text.IndexOf(" ", text.IndexOf("#")) - text.IndexOf("#");
                String top = text.Substring(text.IndexOf("#"), stop);
                text.Remove(text.IndexOf("#"), stop);            
                tempTop.AddLast(top);

            }
            topics = tempTop;
        }

        public void linkSearch(String text) {
            //parse tweet for links, what happens when there are multiple links tho???
            LinkedList<String> tempLink = new LinkedList<String>();
            while (text.IndexOf("http:") != -1 && text.IndexOf("https:") != -1)
            {
                if (text.IndexOf("http:") != -1)
                {
                    int stop = text.IndexOf(" ", text.IndexOf("http:")) - text.IndexOf("http:");
                    String link = text.Substring(text.IndexOf("http:"), stop);
                    text.Remove(text.IndexOf("http:"), stop);
                    tempLink.AddLast(link);
                }
                else if (text.IndexOf("https:") != -1) {
                    int stop = text.IndexOf(" ", text.IndexOf("https:")) - text.IndexOf("https:");
                    String link = text.Substring(text.IndexOf("https:"), stop);
                    text.Remove(text.IndexOf("https:"), stop);
                    tempLink.AddLast(link);
                }

            }
            links = tempLink;



        }

        public void parseTweet (String text)
        {
            //Check Mentions
            mentionSearch(text);
            //Check Topics
            topicSearch(text);
            //Check Links
            linkSearch(text);

        }

    }
}

